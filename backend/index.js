const express = require('express')
const cors = require('cors')
const app = express()
const port = process.env.PORT

let dbResources = JSON.parse(process.env.PGWEB_DB_RESOURCES);

console.log(process.env)

app.use(express.json())
app.use(cors())

app.get('/', (request, response) => {
  let keys = []
  Object.keys(dbResources).forEach(key => {
    keys.push(key)
  });
  response.send(keys)
})

app.post('/', (request, response) => {
  console.log(request.body)
  let b = request.body.resource
  if(dbResources[b] === null || dbResources[b] === undefined)
    response.status(404).send()
  else
    response.send({ 'database_url': dbResources[b] })
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
