# pgweb

## Getting started

Requirements

- docker
- docker-compose

Run

- git clone https://gitlab.com/yriase/pgweb.git
- cd pgweb
- docker-compose up

Test

- Open your browser to [http://localhost:8080](http://localhost:8080)
